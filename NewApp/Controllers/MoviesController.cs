﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NewApp.Models;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

/// <summary>
/// Sapiens Dev 2018
/// Manuel Amaya
/// Strict Rest for Movies, Works directly with the model
/// I a not sure at this point if I will reach by time to handle this, but
/// I like to handle as IHttpActionResult  to get a better control of the responses, anoter advantage is that 
/// the api does not falls loudy
/// </summary>
namespace NewApp.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly SapiensContext _context;

        public MoviesController(SapiensContext context)
        {
            _context = context;
        }

        // GET: api/Movies
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                if (_context.Movie.Any())
                {
                    return Ok(_context.Movie.ToList().OrderBy(x => x.Title));
                }

                return NotFound(new ExceptionModel { ExceptionMessage = "Not records founded" });
            }
            catch (Exception ex)
            {
                return BadRequest(new ExceptionModel
                {
                    ExceptionMessage = "An exception occure, please check the details",
                    Exception = ex
                });
            }

        }

        // GET api/Movies/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                if (_context.Movie.Any(x => x.ID == id))
                {
                    return Ok(_context.Movie.SingleOrDefault(x => x.ID == id));
                }
                return NotFound(new ExceptionModel { ExceptionMessage = $"Not record founded with the id " + id });
            }
            catch (Exception ex)
            {
                return BadRequest(new ExceptionModel
                {
                    ExceptionMessage = "An exception occure, please check the details",
                    Exception = ex
                });
            }
        }

        // POST api/Movies
        [HttpPost]
        public IActionResult Post([FromBody]Movie value)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var movie = new Movie
                    {
                        Genre = value.Genre,
                        Actors = value.Actors,
                        Director = value.Director,
                        Rating = value.Rating,
                        Year = value.Year,
                        Title = value.Title
                    };

                    _context.Movie.Add(movie);
                    _context.SaveChanges();

                    return Get(movie.ID);
                }
                else
                {
                    return BadRequest(new ExceptionModel
                    {
                        ExceptionMessage = "An exception occurs witht the model, check the values"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new ExceptionModel
                {
                    ExceptionMessage = "An exception occure, please check the details",
                    Exception = ex
                });
            }
        }

        // PUT api/Movies/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Movie value)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Movie movie = _context.Movie.Single(x => x.ID == id);
                    movie.Genre = value.Genre;
                    movie.Actors = value.Actors;
                    movie.Director = value.Director;
                    movie.Rating = value.Rating;
                    movie.Year = value.Year;
                    movie.Title = value.Title;
                    _context.SaveChanges();
                }
                else
                {
                    return BadRequest(new ExceptionModel
                    {
                        ExceptionMessage = "An exception occurs witht the model, check the values"
                    });
                }
                return Get(id);
            }
            catch (Exception ex)
            {
                return BadRequest(new ExceptionModel
                {
                    ExceptionMessage = "An exception was thrown, please check the details",
                    Exception = ex
                });
            }
        }

        // DELETE api/Movies/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                Movie movie = _context.Movie.Single(x => x.ID == id);
                _context.Movie.Remove(movie);
                _context.SaveChanges();
                return Ok($"The movie {movie.Title} was permantly removed");
            }
            catch (Exception ex)
            {
                return BadRequest(new ExceptionModel
                {
                    ExceptionMessage = "An exception was thrown, please check the details",
                    Exception = ex
                });
            }
        }
    }
}

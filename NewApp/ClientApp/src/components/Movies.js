﻿import React, { Component } from 'react'
import { Button, Row, Col, Table, Glyphicon, PageHeader} from 'react-bootstrap'

import MovieForm from './MovieForm'
import './Movies.css';

export class Movies extends Component  {    

    constructor(props) {
        super(props)
        this.state = {
            movies: [],
            loading: true,
            currentMovie: {},
            showEdition: false,
            isNewMovie: true,
            apiMessaging: ""
        };
        this.update = this.update.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.openNew = this.openNew.bind(this);
        this.save = this.save.bind(this);
        this.reponseHandling = this.reponseHandling.bind(this);
    }
    
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let movieChange = { [name]: value };
        
        this.setState({
            currentMovie: { ...this.state.currentMovie, ...movieChange }
        });
    }
    
    updateMovieOpen(movie){
        this.setState({ showEdition: true, currentMovie: movie, isNewMovie: false });
    }

    openNew() {
        this.setState({
            currentMovie: {
                title: "",
                genre: "",
                rating: 0,
                actors: "",
                director: "",
                year: "1888"
            },
            isNewMovie: true
        });
    }

    save() {
        debugger;
        return fetch("api/Movies/", {
            method: 'post',
            body: JSON.stringify(this.state.currentMovie),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (!response.ok) {
                if (response.status == 400 || response.status == 500) {
                    debugger;
                    this.setState({ apiMessaging: "the api response was not ok, check the data you are trying to update, the js console can help too" });
                }
            } else {
                return response.json();
            }

            })
            .then(data => { this.getData() })
    }

    update() {
        return fetch(`api/Movies/${this.state.currentMovie.id}`, {
            method: 'put',
            body: JSON.stringify(this.state.currentMovie),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {          
            if (!response.ok) {                
                if (response.status == 400 || response.status == 500 ) {                
                    this.setState({ apiMessaging: "the api response was not ok, check the data you are trying to update, the js console can help too"});                   
                }
            } else {
                return response.json();
            }
          
        })
          .then(data => { this.getData() })
          .catch(data => console.log(data))
    }
    

    deleteMovie(id, title) {
        return fetch(`api/Movies/${id}`, {
            method: 'delete'
        })
            .then(response => response)
            .then(data => { this.getData() })
            .catch(data => console.error(data))
    }

    getData() {        
        fetch('api/Movies')
            .then(response => {
                if (!response.ok) {
                    debugger;
                    if (response) {
                        this.setState({ apiMessaging: "the api response was not ok, the code that returned was " + response.status });
                    }                    
                } else {
                    return response.json()
                }
            })
            .then(data => {
                return (this.setState({ movies: data, loading: false }));
        });
    }
    
    componentDidMount() {
       
        this.getData();        
    }


    reponseHandling(response) {
        if (!response.ok) {
            debugger;
            if (response) {
                this.setState({ apiMessaging: "the api response was not ok, the code that returned was " + response.status });
            }
            return;
        }
        return response.json()
    }

    render() {

        let movies = this.state.movies || [];      

        if (this.state.loading){
            return (<div>Please wait :)</div>)
        }
    
        return (            
            <Row>
                <Col sm={8}>                  
                    <PageHeader>
                        Movies List
                    </PageHeader>
                     
                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Genre</th>
                                <th>Year</th>
                                <th>Director</th>
                                <th>Actor</th>
                                <th><span className="glyphicon glyphicon-star" aria-hidden="true"></span> Rating</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {movies.map(movie =>
                                <tr key={movie.id}>
                                    <td> {movie.title} </td>
                                    <td> {movie.genre} </td>
                                    <td> {movie.year} </td>
                                    <td> {movie.director} </td>
                                    <td> {movie.actors} </td>
                                    <td> {movie.rating} </td>
                                    <td>
                                        <Button onClick={this.updateMovieOpen.bind(this, movie)} bsStyle="warning" bsSize="small">
                                            <Glyphicon glyph="pencil" />
                                        </Button>
                                        <Button onClick={this.deleteMovie.bind(this, movie.id, movie.title)} bsStyle="danger" bsSize="small"> <Glyphicon glyph="trash" /> </Button>                                       
                                    </td>
                                </tr>
                            )}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan="7" className="alert alert-warning" role="alert">
                                    Just information, don't be scared please:<br />
                                    <p className="small" >{this.state.apiMessaging}</p>
                                </td>
                            </tr>
                        </tfoot>
                    </Table>                   
                </Col>
                <Col sm={4} className="right-sidebar">
                    <Button onClick={this.openNew}> Clear Form </Button>
                    <MovieForm movie={this.state.currentMovie}
                        update={this.update}
                        handleInputChange={this.handleInputChange}
                        save={this.save}
                        isNewMovie={this.state.isNewMovie} />
                </Col>
            </Row>
        );
    }
}

﻿import React, { Component } from 'react'
import { Form, FormGroup, Col, FormControl, Button, ControlLabel, Row} from 'react-bootstrap'

class MovieForm extends Component {
   
    render() {
        return (
            <Form horizontal>
                <FormGroup>
                    <Row>
                        <Col md={6}>
                            <ControlLabel>* Title</ControlLabel>
                            <FormControl type="text" name="title" onChange={this.props.handleInputChange} value={this.props.movie.title} required />
                        </Col>
                        <Col md={6}>
                            <ControlLabel>Genre</ControlLabel>
                            <FormControl type="text" name="genre" onChange={this.props.handleInputChange} value={this.props.movie.genre} required/>
                        </Col>                      
                    </Row>
                </FormGroup>

                <FormGroup>
                    <Row>
                        <Col md={6}>
                            <ControlLabel> * Year (can't be in the future)</ControlLabel>
                            <FormControl type="string" name="year" onChange={this.props.handleInputChange} value={this.props.movie.year} />
                        </Col>
                        <Col md={6}>
                            <ControlLabel>Director </ControlLabel>
                            <FormControl type="string" name="director" onChange={this.props.handleInputChange} value={this.props.movie.director} />
                        </Col>                       
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col md={6}>
                            <ControlLabel>* Rating (between 1 and 5) </ControlLabel>
                            <FormControl type="string" name="rating" onChange={this.props.handleInputChange} value={this.props.movie.rating} />
                        </Col>
                        <Col md={6}>
                            <ControlLabel>Actor(s) </ControlLabel>
                            <FormControl type="string" name="actors" onChange={this.props.handleInputChange} value={this.props.movie.actors} />
                        </Col>
                    </Row>
                </FormGroup>           

                <FormGroup>
                    <Col md={12}>
                        {this.props.isNewMovie ?
                            <Button onClick={this.props.save}> Save New </Button> :
                            <Button onClick={this.props.update}> Save Changes</Button>}
                    </Col>
                </FormGroup>
        </Form>);
    }
}

export default MovieForm;
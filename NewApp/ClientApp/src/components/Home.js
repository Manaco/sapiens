import React, { Component } from 'react';

export class Home extends Component {
  displayName = Home.name

  render() {
    return (
      <div>
        <h1>Hello</h1>
        <p>I want to thank you for the opportunity to make this test,
           maybe this could not be the best one, but I consider that this is a great
           way to learn and practice several things that are not usual in my daily job.</p>
        <p> Below, I will include several annotations that I think it is important to consider: </p>
        <ul>
                <li>I am using a seed  and local Db, the project should to create the Db and populate it when it runs. I will attach the
                    Db that is generated to be sure. In my pc, the db is located in "C:\Users\myUser". </li>
                <li>The validations are right now inside the Model layer, that means that if any field  does not
                    cover the specifications, it should be not inserted to the DB.</li>
                <li>Sorry for the spaghetti code in the Movies.js, React is something still new for me. I am aware that
                    there are several good practices that I did not follow around react.</li>
                <li>I am not a bootstrap lover but consider is a good way to reach a decent and quick design.</li>   
                <li>I don't handle as I want the exceptions in the fetch, please open the console in case of weird behavior, the API throws
                    a little bit better explained details when an exception occurs.</li>
                
        </ul>

        <h2> To do </h2>
        <ul>
                <li>Exception Control in Front End</li>
                <li>Warning message before the deletion </li>
                <li>Js code documentation</li>
                <li>Fancy Rating selector</li>
                <li>Fancy Rating display</li>
                <li>Ad more Models </li>
                <li>look for a local generation of the DB</li>
        </ul>

        <p>
            Sincerely, <br/>
            Manuel Amaya
        </p>
      </div>
    );
  }
}

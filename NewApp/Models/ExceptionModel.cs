﻿using System;
namespace NewApp.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ExceptionModel
    {
        public string ExceptionMessage { get; set; }
        public Exception Exception { get; set; }
    }
}

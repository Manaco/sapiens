﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewApp.Models
{
    public class SapiensContext : DbContext
    {
        public SapiensContext(DbContextOptions<SapiensContext> options)
            : base(options)
        { }

        public DbSet<Movie> Movie { get; set; }        
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace NewApp.Models
{

    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SapiensContext(
                serviceProvider.GetRequiredService<DbContextOptions<SapiensContext>>()))
            {
                // Look for any movies.
                if (context.Movie.Any())
                {
                    return;   // DB has been seeded
                }

                context.Movie.AddRange(
                     new Movie
                     {
                         Title = "When Harry Met Sally",
                         Year = 1989,
                         Genre = "Romantic Comedy",
                         Actors = "Adam Sandler",
                         Director= "I dunno",
                         Rating= (float) 1.00
                     },

                     new Movie
                     {
                         Title = "Ghostbusters ",
                         Genre = "Romantic Comedy",
                         Actors = "Adam Sandler",
                         Year = 2000,
                         Director = "I dunno",
                         Rating = (float)1.00
                     }
                     
                );
                context.SaveChanges();
            }
        }
    }


}

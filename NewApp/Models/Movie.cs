﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewApp.Models
{
    /// <summary>
    /// Sapiens Dev 2018
    /// Manuel Amaya
    /// Model to migrate and validate in Controller 
    /// Notes for the evaluator: 
    /// I am used to comment by GhostDoc all the attributes in models or classes, I will ommit them to make
    /// easier and confortable the evaluation, also, the Data Anotations make a great auto - documentation
    /// </summary>
    public class Movie
    {
        
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage ="Please include a title")]
        public string Title { get; set; }

        [Required, Range(0, 2018, ErrorMessage = "Please enter valid integer Year, can't  be in the future")]
        public int Year { get; set; }

        public string Genre { get; set; }

        public string Director { get; set; }

        [Required, Range(1,5, ErrorMessage ="Please enter a valid rating range, should be between 1 and 5")]
        public float Rating { get; set; }

        public string Actors { get; set; }

    }

}
